package com.app.challenge4;

import com.app.challenge4.controller.UserController;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class UserControllerTest {

    @Autowired
    private UserController userController;

    @Test
    @DisplayName("Menambahkan user")
    void createUser() {
        userController.createUser("nomor6", "nomor6@gmail.com", "nomor6123");
    }

    @Test
    @DisplayName("Mengupdate user")
    void updateUser() {
        userController.updateUser("nomor7", "nomor7@gmail.com", "nomor7123", 1);
    }

    @Test
    @DisplayName("Menghapus user")
    void deleteUser() {
        userController.deleteUser(1);
    }
}
