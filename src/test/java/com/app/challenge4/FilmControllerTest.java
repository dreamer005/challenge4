package com.app.challenge4;

import com.app.challenge4.controller.FilmController;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class FilmControllerTest {

    @Autowired
    FilmController filmController;

    @Test
    @DisplayName("Menambahkan user")
    void createUser() {
        filmController.createFilm("CODE1", "film1", 1);
    }

    @Test
    @DisplayName("Mengupdate user")
    void updateUser() {
        filmController.updateFilm("CODE2", "film2", 0, 1);
    }

    @Test
    @DisplayName("Menghapus user")
    void deleteUser() {
        filmController.deleteFilm(1);
    }

    // Menampilkan show yang sedang tayang
    @Test
    @DisplayName("4. Menampilkan Film-film yang sedang tayang")
    void showSedangTayang() {
        filmController.showAllSedangTayang();
    }

    // Menampilkan schedule suatu film
    @Test
    @DisplayName("5. Menampilkan Jadwal suatu film (by id)")
    void showFilmSchedule() {
        filmController.showScheduleFilm(13);
    }

    // Hapus film (plus schedule)
    @Test
    @DisplayName("3. Menghapus film (+schedulefilm)")
    void deleteFilm() {
        filmController.deleteFilm(13);
    }

}
