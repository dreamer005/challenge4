package com.app.challenge4.repository;

import com.app.challenge4.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface UserRepository extends JpaRepository<UserEntity, Integer> {

    @Modifying
    @Query(nativeQuery = true, value = "INSERT INTO users(username, email, password) VALUES(:username, :email, :password)")
    void createUser(
            @Param("username") String username,
            @Param("email") String email,
            @Param("password") String password
    );

    @Modifying
    @Query(nativeQuery = true, value = "UPDATE users u SET u.username= :username, u.email= :email, u.password= :password WHERE u.user_id= :user_id")
    void updateUser(
            @Param("username") String username,
            @Param("email") String email,
            @Param("password") String password,
            @Param("user_id") Integer userId
    );

    @Modifying
    @Query(nativeQuery = true, value = "DELETE FROM users WHERE user_id= :user_id")
    void deleteUser(
            @Param("user_id") Integer userId
    );
}
