package com.app.challenge4.repository;

import com.app.challenge4.entity.FilmEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public interface FilmRepository extends JpaRepository<FilmEntity, Integer> {

    @Query(nativeQuery = true, value = "SELECT COUNT(1) FROM films")
    Integer countFilms();

    // Add Film
    @Modifying
    @Query(nativeQuery = true, value = "insert into films(film_code, film_name, showing) values(:film_code, :film_name, :showing)")
    void createFilm(
            @Param("film_code") String filmCode,
            @Param("film_name") String filmName,
            @Param("sedang_tayang") Integer showing
    );

    // Update Film data
    @Modifying
    @Query(nativeQuery = true, value = "update films f set f.film_code= :film_code, f.film_name= :film_name, f.showing= :showing where f.film_id= :film_id")
    void updateFilm(
            @Param("film_code") String filmCode,
            @Param("film_name") String filmName,
            @Param("sedang_tayang") Integer showing,
            @Param("film_id") Integer filmId
    );

    // Delete a film
    @Modifying
    @Query(nativeQuery = true, value = "delete from films where film_id= :film_id")
    void deleteFilm(
            @Param("film_id") Integer filmId
    );

    // Delete film schedule
    @Modifying
    @Query(nativeQuery = true, value = "delete from schedules where film_id= :film_id")
    void deleteScheduleFilm(
            @Param("film_id") Integer filmId
    );


    @Query(nativeQuery = true, value = "select s.* from films f join schedules s on f.film_id = s.film_id where f.film_id = :film_id")
    List<Object[]> showFilmSchedule(
            @Param("film_id") Integer filmId
    );

}