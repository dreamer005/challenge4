package com.app.challenge4.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Setter
@Getter
@Embeddable
public class SeatNumber implements Serializable {

    private Integer seatNumber;

    private String filmName;
    private String date;
    private String start;

    public SeatNumber() {
    }

    public SeatNumber(String filmName, String date, String start) {
        this.filmName = filmName;
        this.date = date;
        this.start = start;
    }
}
