package com.app.challenge4.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Setter
@Getter
@Entity
@Table(name = "seats")
public class SeatEntity {

    @Column(name = "studio_name")
    private String studioName;

    @EmbeddedId
    @Column(name = "seat_number")
    private SeatNumber seatNumber;

}
