package com.app.challenge4.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Setter
@Getter
@Entity
@Table(name = "schedules")
public class ScheduleEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "schedule_id")
    public Integer scheduleId;

    @ManyToOne(targetEntity = FilmEntity.class)
    @JoinColumn(name = "film_id")
    private Integer filmId;

    @Column(name = "date")
    private String date;
    @Column(name = "start_datetime")
    private String startDateTime;
    @Column(name = "end_datetime")
    private String endDateTime;
    @Column(name = "ticket_price")
    private Integer ticketPrice;

    public ScheduleEntity() {
    }

    public ScheduleEntity(Integer scheduleId, Integer filmId, String date, String start_datetime, String end_datetime, Integer ticket_price) {
        this.scheduleId = scheduleId;
        this.filmId = filmId;
        this.date = date;
        this.startDateTime = startDateTime;
        this.endDateTime = endDateTime;
        this.ticketPrice = ticketPrice;
    }

}
