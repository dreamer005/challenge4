package com.app.challenge4.service;

import com.app.challenge4.entity.FilmEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface FilmServiceI {

    public Integer countFilms();
    void createFilm(String filmCode, String filmName, Integer showing);
    void updateFilm(String filmCode, String filmName, Integer showing, Integer filmId);
    void deleteFilm(Integer filmId);
    void deleteScheduleFilm(Integer filmId);
    List<FilmEntity> showAllFilms();
    List<Object[]> showScheduleFilm(Integer filmId);


}
