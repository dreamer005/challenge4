package com.app.challenge4.service;

import com.app.challenge4.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UsersService implements UserServiceI {

    @Autowired
    private UserRepository userRepository;

    @Override
    public void createUser(String username, String email, String password) {
        userRepository.createUser(username, email, password);
    }

    @Override
    public void updateUser(String username, String email, String password, Integer userId) {
        userRepository.updateUser(username, email, password, userId);
    }

    @Override
    public void deleteUser(Integer userId) {
        userRepository.deleteUser(userId);
    }
}
