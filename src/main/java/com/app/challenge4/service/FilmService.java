package com.app.challenge4.service;

import com.app.challenge4.entity.FilmEntity;
import com.app.challenge4.repository.FilmRepository;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Service
public class FilmService implements FilmServiceI {

    @Autowired
    private FilmRepository filmRepository;

    @Override
    public Integer countFilms() {
        return filmRepository.countFilms();
    }


    public FilmService(FilmRepository filmRepository) {
        this.filmRepository = filmRepository;
    }

    @Override
    public void createFilm(String filmCode, String filmName, Integer showing) {
        filmRepository.createFilm(filmCode, filmName, showing);
    }

    @Override
    public void updateFilm(String filmCode, String filmName, Integer showing, Integer filmId) {
        filmRepository.updateFilm(filmCode, filmName, showing, filmId);
    }

    @Override
    public void deleteFilm(Integer filmId) {
        filmRepository.deleteFilm(filmId);
    }

    @Override
    public void deleteScheduleFilm(Integer filmId) {
        filmRepository.deleteScheduleFilm(filmId);
    }

    @Override
    public List<FilmEntity> showAllFilms() {
        return filmRepository.findAll();
    }

    @Override
    public List<Object[]> showScheduleFilm(Integer filmId) {
        return filmRepository.showFilmSchedule(filmId);
    }

}
