package com.app.challenge4.service;

import org.springframework.stereotype.Service;

@Service
public interface UserServiceI {

    void createUser(String username, String email, String password);
    void updateUser(String username, String email, String password, Integer userId);
    void deleteUser(Integer userId);

}
