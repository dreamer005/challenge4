package com.app.challenge4.controller;

import com.app.challenge4.entity.FilmEntity;
import com.app.challenge4.entity.ScheduleEntity;
import com.app.challenge4.service.FilmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.util.ArrayList;
import java.util.List;

@Controller
public class FilmController {

    @Autowired
    private FilmService filmService;

    public void countFilms() {
        System.out.println(filmService.countFilms());
    }

    public void createFilm(String filmCode, String filmName, Integer showing){
        filmService.createFilm(filmCode, filmName, showing);
    }

    public void updateFilm(String filmCode, String filmName, Integer showing, Integer filmId) {
        filmService.updateFilm(filmCode, filmName, showing, filmId);
    }

    public void deleteFilm(Integer filmId) {
        filmService.deleteScheduleFilm(filmId);
        filmService.deleteFilm(filmId);
    }

    public void showAllSedangTayang() {
        List<FilmEntity> allFilms = filmService.showAllFilms();

        allFilms.forEach(filmEntity -> {
            if (filmEntity.getShowing() == 1) {
                System.out.println(filmEntity.getFilmName());
            }
        });
    }

    public void showScheduleFilm(Integer filmId) {
        List<Object[]> obj = filmService.showScheduleFilm(filmId);
        List<ScheduleEntity> lSE = new ArrayList<>();
        for (Object[] objects : obj) {
            ScheduleEntity se = new ScheduleEntity((Integer) objects[0], (Integer) objects[1], (String) objects[2], (String) objects[3], (String) objects[4], (Integer) objects[5]);
            lSE.add(se);
        }

        lSE.forEach(entity -> {
            System.out.println("Tanggal tayang      : " + entity.getDate());
            System.out.println("Jam mulai           : " + entity.getStartDateTime());
            System.out.println("Jam selesai         : " + entity.getEndDateTime());
            System.out.println("Harga tiket         : Rp. " + entity.getTicketPrice());
        });
    }

}
