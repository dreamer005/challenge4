package com.app.challenge4.controller;

import com.app.challenge4.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class UserController {

    @Autowired
    private UsersService usersService;

    public void createUser(String username, String email, String password) {
        usersService.createUser(username, email, password);
    }

    public void updateUser(String username, String email, String password, Integer userId) {
        usersService.updateUser(username, email, password, userId);
    }

    public void deleteUser(Integer userId) {
        usersService.deleteUser(userId);
    }
}
